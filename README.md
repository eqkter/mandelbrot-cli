# Mandelbrot-CLI

Just the Mandelbrot's fractal set (50 iterations) directly prompted via CLI. The purpose of this project is to do it in various language, a sort of an advanced *Hello World!*

- Ada: `gprbuild mandelbrot.adb && ./mandelbrot` (dependence: [GNAT](https://www.gnu.org/software/gnat/))
- C: `gcc -o mandelbrot.o mandelbrot.c -lm && ./mandelbrot.o`
- Java: `javac mandelbrot.java && java mandelbrot` (thx to [Juergen Key](https://framagit.org/sgjfsf))
- Julia: `julia mandelbrot.jl`
- Octave: `octave mandelbrot.m`
- Perl: `perl mandelbrot.pl`
- Python: `python3 mandelbrot.py` (dependence: `pip3 install --user numpy`)
- Ruby: `ruby mandelbrot.rb`
