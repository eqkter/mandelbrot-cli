with Ada.Numerics.Generic_Complex_Types;
with Ada.Text_IO;

procedure Mandelbrot is

	type My_Real is digits 15;  --  Double precision
	package RC is new Ada.Numerics.Generic_Complex_Types (My_Real);
	use RC;
   	package Txt renames Ada.Text_IO;

	function Suite (a : Complex) return Complex is
		z : Complex;
	begin
		z := 0.0 + 0.0*i;
		for I in 0 .. 50 loop
			z := z*z + a;
		end loop;
		return z;
	end Suite;

	x,y : My_Real;
	c : Complex;
begin
	y := 1.0;
	while y >= -1.0 loop
		x := -2.0;
		while x <= 0.5 loop
			c := (Re => x, Im => y);
			if Modulus(Suite(c)) < 2.0 then
				Txt.Put("*");
			else 
				Txt.Put(" ");
			end if;
			x := x + 0.0315;
		end loop;
		Txt.New_Line;
		y := y - 0.05;
	end loop;
end Mandelbrot;
