#include <stdio.h>
#include <complex.h>

double complex mandelbrot(double complex c){
	double complex z = 0 + 0*I;
	for (size_t i = 0; i < 50; ++i) {
		z = z*z + c;
	}
	return z;
}

int main(int argc, char *argv[])
{
	double complex X;
	double A;

	for (double y = 1.0; y >= -1.0; y-=0.05) {
		for (double x = -2.0; x <= 0.5; x+=0.0315) {
			X = mandelbrot(x+y*I);
			A = sqrt(creal(X)*creal(X) + cimag(X)*cimag(X));
			if (A < 2) {printf("*");}
			else {printf(" ");}		
		}
		printf("\n");
	}
	return 0;
}
