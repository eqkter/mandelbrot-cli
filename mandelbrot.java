public class mandelbrot
{
	private static final java.awt.geom.Point2D ORIGIN=new java.awt.geom.Point2D.Double(0,0);

	private static java.awt.geom.Point2D Mandelbrot(double re, double im, int iterations)
	{
		double zre = 0;
		double zim = 0;
		for (int i = 0; i < iterations; ++i)
		{
			double sqre=zre*zre-zim*zim;
			double sqim=zre*zim*2;
			zre=sqre+re;
			zim=sqim+im;
		}
		return new java.awt.geom.Point2D.Double(zre,zim);
	}

	public static void main(java.lang.String[] args)
	{
		int iterations=50;
		if(args.length>0)
		{
			try{
			iterations=java.lang.Integer.parseInt(args[0]);
			}catch(java.lang.Throwable t){}
		}
		java.awt.geom.Point2D X;
		double A;

		for (double y = 1.0; y >= -1.0; y-=0.05) {
			for (double x = -2.0; x <= 0.5; x+=0.0315) {
				X = Mandelbrot(x,y,iterations);
				A = X.distance(ORIGIN);
				if (A < 2) {System.out.print("*");}
				else {System.out.print(" ");}
			}
			System.out.println();
		}

	}
}
