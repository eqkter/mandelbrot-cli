%!usr/bin/octave

close all; clear, clc

function [z] = suite(c)
	z = 0;
	for i = 1:50
		z = z.^2 + c;
	end
end

x = -2:0.0315:0.5;
y = 1:-0.05:-1;
[X,Y] = meshgrid(x,y);

A = (abs(suite(X+Y*1i)) < 2);
[rows,cols] = size(A);
S = zeros(cols);

for ii = 1:rows
	for jj = 1:cols
		if A(ii,jj)
			S(jj) = '*';
		else
			S(jj) = ' ';
		end
	end
	fprintf("%s\n",S)
end
