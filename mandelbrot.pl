#!/usr/bin/perl

use Math::Complex;

sub mandelbrot {
	$c = $_[0]; # arg
	$z = 0;
	foreach (0 .. 50) {
		$z = $z**2 + $c; 
		# Perl has a hard time with loops and large numbers,
		# so we just check the divergence:
		last if (abs($z) > 2);	}
	return $z;
}

for ($y = 1.0; $y > -1.05; $y -= 0.05) {
	for ($x = -2.0; $x < 0.5; $x += 0.0315) {
		if (abs(mandelbrot($x+$y*i)) < 2) {print "*";}
		else {print " ";}
	}
	print "\n";
}
