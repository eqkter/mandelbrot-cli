#!/usr/bin/python3

from math import *
import numpy as np

def mandelbrot(c) :
    z = complex(0,0)
    for i in range(0,50) :
        z = z*z + c
    return z

for y in np.arange(1.0,-1.05,-0.05) :
    for x in np.arange(-2.0,0.5315,0.0315) :
        A = abs(mandelbrot(complex(x,y)))
        if A < 2 : print("*",end="")
        else : print(" ",end="")
    print("")
