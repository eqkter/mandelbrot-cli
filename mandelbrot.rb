#!~/.rvm/rubies/ruby-3.0.0/bin/ruby

def mandelbrot(c)
  z = 0
  for i in Range.new(1,51)
    z = z*z + c
  end
  return z
end

def amp(z)
  return (z.real**2 + z.imag**2) ** 0.5
end

def range(start, stop, step)
  acc = []
  until start >= stop
    acc.append(start)
    start += step.to_f
  end
  return acc
end

for y in range(-1.0,1.0,'0.05')
  for x in range(-2.0,0.5,'0.0315')
    if amp(mandelbrot(Complex(x,y))) < 2
      print "*"
    else 
      print " "
    end
  end
  puts ""
end
